from flask import Flask, url_for
from flask import Flask, jsonify, request, render_template, Response
from flask_socketio import send, emit
from flask_socketio import SocketIO
import time
import math

from connection_manager import ConnectionManager
from data_model import DevicePosition, Event

app = Flask(__name__)
socketio = SocketIO(app)
connection_manager = ConnectionManager(socketio=socketio)
device_positions = {}
active_events = []
device_socketid_map = {}


@app.route('/')
def api_root():
    return 'Welcome'


@app.route('/articles')
def api_articles():
    return 'List of ' + url_for('api_articles')


@app.route('/articles/<articleid>')
def api_article(articleid):
    return 'You are reading ' + articleid


@socketio.on('message')
def handle_message(message):
    print message
    # emit()
    # send(message)


# @socketio.on('json')
# def handle_json(json):
#     send(json, json=True)


# @socketio.on('my event')
# def handle_my_custom_event(json):
#     emit('my response', json)
#

# @socketio.on('test_location')
# def handle_my_custom_event(lat, lng):
#     print('received test location: ' + str(lat) + ', ' + str(lng))


@socketio.on('test_location_sid')
def handle_my_custom_event(lat, lng):
    print 'sid test :%s, (%s, %s)' % (str(request.sid), str(lat), str(lng))


@socketio.on('device_location')
def handle_device_location(device_id, latitude, longitude, speed=50):
    print 'Received location for device: %s, lat: %s, long: %s' % (device_id, str(latitude), str(longitude))
    connection_manager.update_device_sid(device_id, request.sid)
    if not device_positions.get(device_id):
        device_positions[device_id] = DevicePosition(device_id)

    device_position = device_positions[device_id]
    device_position.set_position(latitude, longitude, speed)
    print 'Course: %s' % device_position.course
    handle_location_for_alerts(device_id, device_position)
    handle_location_for_atc(device_id, device_position)
    emit_position_stream(device_id, device_position)
    cleanup_events()


@socketio.on('device_event')
def handle_event(latitude, longitude, radius, priority, description=''):
    print('received events: ' + str(latitude) + ', ' + str(longitude))
    event = Event(latitude=latitude, longitude=longitude, radius=radius, priority=priority, description=description)
    active_events.append(event)
    emit_event_stream(event)


@socketio.on('connect')
def test_connect():
    print 'socket connected: %s' % request.sid
    # emit('message', 'Connected to server')


@socketio.on('disconnect')
def test_disconnect():
    print 'Socket disconnected: %s' % request.sid


def emit_position_stream(device_id, device_position):
    print 'emit position for device id: %s' % device_id
    socketio.emit('position_stream', (device_id, device_position.position.latitude,
                             device_position.position.longitude,
                             device_position.speed,
                             device_position.course) + device_position.roi_point_tuple())
    # emit('position_stream', data=(device_id))


def emit_event_stream(event):
    socketio.emit('event_stream', (id(event), event.position.latitude, event.position.longitude,
                                   event.radius, event.priority, event.description))
    socketio.emit('test_stream', ('string', 'str2'))


def emit_event_deletion(event):
    socketio.emit('event_deletion_stream', id(event))


def handle_location_for_alerts(device_id, position):
    # alert_events = []
    for event in active_events:
        # TODO: update this to use roi polygon
        if position.point_in_roi(event.position) and not event.is_device_notified(device_id):
            print 'Notify deviceId: %s for eventId: %s' % (device_id, id(event))
            connection_manager.notify_event_to_device(device_id, event)
            event.set_device_notified(device_id)

acc_devices = []
def handle_location_for_atc(current_device_id, current_device_position):
    for device_id, device_position in device_positions.iteritems():
        if current_device_position.position.distance_from(device_position.position) < 100 and \
                device_position.speed > 30 and \
                current_device_position.speed > 30 and current_device_id != device_id:
            bearing1 = current_device_position.course
            bearing2 = device_position.course
            diff = abs(bearing1 - bearing2)
            acc_device_str = '%s_%s' % (current_device_id, device_id)
            if acc_device_str not in acc_devices:
                acc_devices.append(acc_device_str)
                # if abs(diff - math.pi/2) < math.pi/8 or True:
                # send alerts to both the devices
                event = Event(current_device_position.position.latitude,
                              current_device_position.position.longitude,
                              'Possible Collision !!',
                              30, 1)
                connection_manager.notify_event_to_device(current_device_id, event);
                connection_manager.notify_event_to_device(device_id, event)
                emit_event_stream(event)


def cleanup_events():
    print 'cleaning up events'
    current_time = int(time.time() * 1000)
    new_active_events = []
    for event in active_events:
        if event.expiration_time < current_time:
            emit_event_deletion(event)
        else:
            new_active_events.append(event)
    del active_events[:]
    active_events.extend(new_active_events)
    # threading.Timer(10, cleanup_events).start()


# t = timer.Timer(1000, lambda: cleanup_events())
# t.start()
# cleanup_events()


if __name__ == '__main__':
    socketio.run(app)
