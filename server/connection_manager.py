
class ConnectionManager:

    device_sid_map = {}

    def __init__(self, socketio):
        self.socketio = socketio
        pass

    def update_device_sid(self, device_id, sid):
        current_sid = self.device_sid_map.get(device_id)
        if current_sid:
            self.socketio.server.leave_room(current_sid, device_id)
        self.device_sid_map[device_id] = sid
        self.socketio.server.enter_room(sid, device_id)
        print 'Add sid: %s to room: %s' % (sid, device_id)

    def get_device_sid(self, device_id):
        return self.device_sid_map.get(device_id)

    def delete_device_id(self, device_id):
        self.device_sid_map[device_id] = None

    def notify_event_to_device(self, device_id, event):
        if self.device_sid_map.get(device_id):
            self.socketio.emit('client_event', (event.position.latitude, event.position.longitude,
                               event.priority, event.description, event.expiration_time),
                               room=device_id)