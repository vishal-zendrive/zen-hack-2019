from util import *
from shapely.geometry import Point, Polygon
import time

class Position:
    def __init__(self, latitude, longitude):
        self.latitude = latitude
        self.longitude = longitude

    def distance_from(self, position):
        """

        :param position: Position
        :return:
        """
        return haversine_distance(self.latitude, self.longitude,
                                  position.latitude, position.longitude)


class DevicePosition:
    position = Position(0, 0)
    device_id = None
    course = 0.0
    speed = 0
    roi = [] # topleft, bottom right

    def __init__(self, device_id):
        self.device_id = device_id

    def set_position(self, latitude, longitude, speed):
        bearing = get_bearing(start_latitude=self.position.latitude,
                                  start_longitude=self.position.longitude,
                                  end_latitude=latitude,
                                  end_longitude=longitude)

        if abs(latitude - self.position.latitude) < .000000001 and abs(longitude - self.position.longitude) < .000000001:
            self.course = bearing

        lat_diff = abs(latitude - self.position.latitude)
        long_diff = abs(longitude - self.position.longitude)

        lat_diff = .0001
        long_diff = .0001

        self.position = Position(latitude=latitude,
                                 longitude=longitude)
        self.speed = speed

        if not speed:
            speed = 10

        del_4 = 0.0012
        del_1 = 0.0003
        sin_course = math.sin(self.course)
        cos_course = math.cos(self.course)
        x_0 = self.position.longitude
        y_0 = self.position.latitude
        p1 = Position(y_0 - del_1 * sin_course + del_4 * cos_course,
            x_0 + del_1 * cos_course + del_4 * sin_course)

        p2 = Position(y_0 - del_1 * sin_course - del_1 * cos_course,
            x_0 + del_1 * cos_course - del_1 * sin_course)

        p3 = Position(y_0 + del_1 * sin_course - del_1 * cos_course,
            x_0 - del_1 * cos_course - del_1 * sin_course)

        p4 = Position(y_0 + del_1 * sin_course + del_4 * cos_course,
            x_0 - del_1 * cos_course + del_4 * sin_course)

        self.roi_points = [p1, p2, p3, p4]

        # set new ROI
        self.roi = Polygon(( (self.roi_points[0].latitude, self.roi_points[0].longitude),
                             (self.roi_points[1].latitude, self.roi_points[1].longitude),
                             (self.roi_points[2].latitude, self.roi_points[2].longitude),
                             (self.roi_points[3].latitude, self.roi_points[3].longitude) ))

    def point_in_roi(self, position):

        point = Point(position.latitude, position.longitude)
        return self.roi.contains(point)

    def roi_point_tuple(self):
        return (self.roi_points[0].latitude, self.roi_points[0].longitude,
                self.roi_points[1].latitude, self.roi_points[1].longitude,
                self.roi_points[2].latitude, self.roi_points[2].longitude,
                self.roi_points[3].latitude, self.roi_points[3].longitude)

class Event:
    """
    This will go to everyone nearby
    """
    description = ''
    position = Position(0, 0)
    latitude = 0.0
    notified_devices = []  # device ids to which the notification has been sent

    def __init__(self, latitude, longitude, description, radius, priority):
        self.position = Position(latitude, longitude)
        self.description = description
        self.radius = radius
        self.priority = priority
        self.expiration_time = int(time.time() * 1000) + 20000

    def is_device_notified(self, device_id):
        return device_id in self.notified_devices

    def set_device_notified(self, device_id):
        self.notified_devices.append(device_id)