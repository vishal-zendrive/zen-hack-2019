import math

def get_bearing(start_latitude, start_longitude, end_latitude, end_longitude):
    """
    returns angle of direction vector, created  by joining two points. its a degree from north
    line in clockwise direction.

    The formulae used is the following:
    theta = atan2(sin(delta long).cos(lat2),
            cos(lat1).sin(lat2) - sin(lat1).cos(lat2).cos(delta long))
    code link -> https://gist.github.com/jeromer/2005586
    :param float start_latitude: first point's latitude.
    :param float start_longitude: first point's longitude.
    :param float end_latitude: second point's latitude.
    :param float end_longitude: second point's longitude.
    :rtype : float
    :return returns angle in degrees from north line in clockwise direction.
    """

    lat1 = math.radians(start_latitude)
    lat2 = math.radians(end_latitude)

    diffLong = math.radians(end_longitude - start_longitude)

    x = math.sin(diffLong) * math.cos(lat2)
    y = math.cos(lat1) * math.sin(lat2) - (math.sin(lat1)
                                           * math.cos(lat2) * math.cos(diffLong))

    initial_bearing = math.atan2(x, y)

    return initial_bearing
    # Now we have the initial bearing but math.atan2 return values
    # from -180 to + 180 degree which is not what we want for a compass bearing
    # The solution is to normalize the initial bearing as shown below
    initial_bearing = math.degrees(initial_bearing)
    compass_bearing = (initial_bearing + 360) % 360

    return initial_bearing, compass_bearing


def haversine_distance(lat1, lon1, lat2, lon2):
    """
    Haversine distance between 2 points on the map.
    :param float lat1: Latitude in degress of first point.
    :param float lon1: Longitude in degress of first point.
    :param float lat2: Latitude in degress of second point.
    :param float lon2: Longitude in degress of second point.
    :return: Distance in metres.
    :rtype: float
    """
    radius = 6371000

    dlat = math.radians(lat2-lat1)
    dlon = math.radians(lon2-lon1)
    a = math.sin(dlat/2) * math.sin(dlat/2) + math.cos(math.radians(lat1)) \
        * math.cos(math.radians(lat2)) * math.sin(dlon/2) * math.sin(dlon/2)
    c = 2 * math.atan2(math.sqrt(a), math.sqrt(1-a))
    d = radius * c

    return d