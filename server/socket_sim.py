import socketio

# standard Python
sio = socketio.Client()


@sio.on('connect')
def on_connect():
    print('Im connected!')


@sio.on('message')
def on_message(data):
    print('I received a message!')


@sio.on('disconnect')
def on_disconnect():
    print('Im disconnected!')

if __name__ == '__main__':
    sio.connect('http://localhost:5000')
    sio.wait()
# sio.emit('message', {'foo': 'bar'})