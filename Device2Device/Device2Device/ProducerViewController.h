//
//  ProducerViewController.h
//  Device2Device
//
//  Created by Atul Manwar on 18/01/19.
//  Copyright © 2019 Zendrive Inc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>

@protocol TapLocationDelegate <NSObject>

- (void)tapDetectedAt:(CLLocationCoordinate2D)location;

@end


@interface ProducerViewController : UIViewController <MKMapViewDelegate, UIGestureRecognizerDelegate, TapLocationDelegate>

@property (weak, nonatomic) IBOutlet MKMapView *mapView;

@end

