//
//  ProducerViewController.m
//  Device2Device
//
//  Created by Atul Manwar on 18/01/19.
//  Copyright © 2019 Zendrive Inc. All rights reserved.
//

#import "ProducerViewController.h"
#import "SocketIOManager.h"
#import "HGestureRecognizer.h"
#import "HConstants.h"
#import "ZDRPeriodicTask.h"

static const NSString *ACCIDENT = @"ACCIDENT";
static const NSString *SNOW = @"Slippery Snow Ahead";
static const NSString *JAM = @"TRAFFIC JAM";
static const NSString *WEDDING = @"Wedding Procession";

@interface ProducerViewController ()
@property (nonatomic, strong) SocketIOManager* manager;

@property (nonatomic) NSMutableArray<MKPointAnnotation *> *eventAnnotations;
@property (nonatomic) MKPointAnnotation *homeLocationAnnotation;

@property (nonatomic) CLLocationCoordinate2D currentLocation;

@property (weak, nonatomic) IBOutlet UIButton *sendEvent;
@property (weak, nonatomic) IBOutlet UIButton *resetLocation;

@property (nonatomic) NSMutableArray<ZDRPeriodicTask *> *annotationRemovalTasks;

@property (nonatomic) HGestureRecognizer *gestureRecognizer;

@end

@implementation ProducerViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
}

- (void)viewDidAppear:(BOOL)animated {
    _homeLocationAnnotation = [[MKPointAnnotation alloc] init];
    _homeLocationAnnotation.coordinate = BEGIN_LOCATION;
    _eventAnnotations = [[NSMutableArray alloc] init];
    _annotationRemovalTasks = [[NSMutableArray alloc] init];
    _gestureRecognizer = [[HGestureRecognizer alloc] initWithDelegate:self andMapView:self.mapView];

    self.mapView.delegate = self;
    self.mapView.showsUserLocation = YES;

    self.manager = [[SocketIOManager alloc] init];
    [self connectToSocket];
    [self moveToBeginLocation];
}

- (void)viewWillDisappear:(BOOL)animated {
    [self.manager disconnect];
    _eventAnnotations = nil;
    _homeLocationAnnotation = nil;
    for (ZDRPeriodicTask *task in _annotationRemovalTasks) {
        [task cancel];
    }
    _annotationRemovalTasks = nil;
    _gestureRecognizer = nil;
}

- (void)tapDetectedAt:(CLLocationCoordinate2D)location {
    [self moveToLocation:location];
    [self eventAlert];
}

- (void)connectToSocket {
    [self.manager connectToSocket];
}

- (void)moveToBeginLocation {
    _currentLocation = BEGIN_LOCATION;
    [self moveToLocation:_currentLocation];
    [self addAnnotation:@"Home" atLocation:_currentLocation];
}

- (void)moveToLocation:(CLLocationCoordinate2D)location {
    _currentLocation = location;
    float spanX = SPAN_PER_HALF_MILE * 2;
    float spanY = SPAN_PER_HALF_MILE * 2;

    MKCoordinateRegion region = self.mapView.region;
    region.center = location;
    region.span = MKCoordinateSpanMake(spanX, spanY);

    [self.mapView.userLocation setCoordinate:region.center];
    [self.mapView setRegion:region animated:YES];
    //self.homeLocationAnnotation.coordinate = location;
    //[self.mapView addAnnotation:self.homeLocationAnnotation];
}

- (IBAction)sendEventAction:(id)sender {
    [self eventAlert];
}

- (void)eventAlert {
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"Select Event" message:@"What's happening here?" preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [self dismissViewControllerAnimated:YES completion:nil];
    }];
    UIAlertAction* accident = [UIAlertAction actionWithTitle:ACCIDENT style:UIAlertActionStyleDefault handler:^(UIAlertAction *action)
                               {
                                   [self dismissViewControllerAnimated:YES completion:nil];
                                   [self sendEventViaSocket:ACCIDENT];
                                   [self addAnnotation:ACCIDENT
                                            atLocation:self.currentLocation];
                               }];
    UIAlertAction* snow = [UIAlertAction actionWithTitle:SNOW style:UIAlertActionStyleDefault handler:^(UIAlertAction *action)
                           {
                               [self dismissViewControllerAnimated:YES completion:nil];
                               [self sendEventViaSocket:SNOW];
                               [self addAnnotation:SNOW
                                        atLocation:self.currentLocation];
                           }];
    UIAlertAction* jam = [UIAlertAction actionWithTitle:JAM style:UIAlertActionStyleDefault handler:^(UIAlertAction *action)
                          {
                              [self dismissViewControllerAnimated:YES completion:nil];
                              [self sendEventViaSocket:JAM];
                              [self addAnnotation:JAM
                                       atLocation:self.currentLocation];
                          }];
    UIAlertAction* barat = [UIAlertAction actionWithTitle:WEDDING style:UIAlertActionStyleDefault handler:^(UIAlertAction *action)
                            {
                                [self dismissViewControllerAnimated:YES completion:nil];
                                [self sendEventViaSocket:WEDDING];
                                [self addAnnotation:WEDDING
                                         atLocation:self.currentLocation];
                            }];
    [alertController addAction:accident];
    [alertController addAction:snow];
    [alertController addAction:jam];
    [alertController addAction:barat];
    [alertController addAction:cancelAction];
    [self presentViewController:alertController animated:YES completion:nil];
}

- (void)addAnnotation:(NSString *)annotationStr
           atLocation:(CLLocationCoordinate2D)location {
    MKPointAnnotation *pointAnnotation = [[MKPointAnnotation alloc] init];
    pointAnnotation.coordinate = location;
    pointAnnotation.title = annotationStr;
    [self.mapView addAnnotation:pointAnnotation];
    [self.eventAnnotations addObject:pointAnnotation];

    __weak ProducerViewController *strongSelf = self;
    ZDRPeriodicTask *task = [ZDRPeriodicTask scheduleBlock:^(ZDRPeriodicTask *task) {
        if (!strongSelf || pointAnnotation == strongSelf.homeLocationAnnotation) {
            return ;
        }
        [self.mapView removeAnnotation:pointAnnotation];
        [strongSelf.eventAnnotations removeObject:pointAnnotation];
    } onQueue:dispatch_get_main_queue() afterDelaySeconds:60];

    [self.annotationRemovalTasks addObject:task];
}

- (void)sendEventViaSocket:(NSString *)description {
    CLLocationCoordinate2D currentLocation = CLLocationCoordinate2DMake(self.currentLocation.latitude,
                                                                        self.currentLocation.longitude);
    [self.manager sendToChannel:DEVICE_EVENT_CHANNEL data:@[@(currentLocation.latitude), @(currentLocation.longitude), @(100), @(1), description]];
}

- (IBAction)resetLocationAction:(id)sender {
    [self moveToBeginLocation];
}

@end
