//
//  ClientEvent.h
//  Device2Device
//
//  Created by Sudeep Kumar on 19/01/19.
//  Copyright © 2019 Zendrive Inc. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreLocation/CLLocation.h>

NS_ASSUME_NONNULL_BEGIN

@interface ClientEvent : NSObject


@property (nonatomic) CLLocationCoordinate2D coordinate;
@property (nonatomic) int priority;
@property (nonatomic, strong) NSString *eventDescription;
@property (nonatomic) int expirationTime;

@end

NS_ASSUME_NONNULL_END
