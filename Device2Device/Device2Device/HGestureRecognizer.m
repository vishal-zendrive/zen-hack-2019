//
//  HGestureRecognizer.m
//  Device2Device
//
//  Created by Sudeep Kumar on 18/01/19.
//  Copyright © 2019 Zendrive Inc. All rights reserved.
//

#import "HGestureRecognizer.h"
#import "ProducerViewController.h"
#import <Foundation/Foundation.h>

@interface HGestureRecognizer ()

@property (nonatomic) MKMapView *mapView;
@property (nonatomic) UITapGestureRecognizer *gestureRecognizer;

@end

@implementation HGestureRecognizer

- (instancetype)initWithDelegate:(id<TapLocationDelegate>)tapDelegate
                      andMapView:(MKMapView *)mapView {
    self = [super init];

    if (self) {
        _tapDelegate = tapDelegate;
        _mapView = mapView;

        _gestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleGesture:)];
        _gestureRecognizer.delegate = self;
        [self.mapView addGestureRecognizer:_gestureRecognizer];
    }

    return self;
}

- (void)handleGesture:(UIGestureRecognizer *)gestureRecognizer {
    if (gestureRecognizer.state != UIGestureRecognizerStateEnded)
        return;

    CGPoint touchPoint = [gestureRecognizer locationInView:self.mapView];
    CLLocationCoordinate2D touchMapCoordinate = [self.mapView convertPoint:touchPoint toCoordinateFromView:self.mapView];
    [self.tapDelegate tapDetectedAt:touchMapCoordinate];
}

@end
