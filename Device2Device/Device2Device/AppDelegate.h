//
//  AppDelegate.h
//  Device2Device
//
//  Created by Atul Manwar on 18/01/19.
//  Copyright © 2019 Zendrive Inc. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

