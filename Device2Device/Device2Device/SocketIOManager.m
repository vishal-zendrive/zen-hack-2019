//
//  SocketIOManager.m
//  Device2Device
//
//  Created by Atul Manwar on 18/01/19.
//  Copyright © 2019 Zendrive Inc. All rights reserved.
//

#import "SocketIOManager.h"
#import "ZDRPeriodicTask.h"
#import "ClientEvent.h"
#import "ConsumerViewController.h"

@implementation SocketIOManager
//static NSString *socketUrlPath = @"http://127.0.0.1:5000";
static NSString *socketUrlPath = @"https://0fafa215.ngrok.io";
//static NSString *socketUrlPath = @"https://e22aa354.ngrok.io";

- (instancetype)initWithURLPath:(NSString *)path {
    self = [super init];
    if (self) {
        NSURL* url = [[NSURL alloc] initWithString:path];
        _manager = [[SocketManager alloc] initWithSocketURL:url config:@{@"log": @YES, @"compress": @YES}];
        _socket = _manager.defaultSocket;
        _manager.reconnectWait = _manager.reconnectWaitMax = 1;
        _isTeardown = NO;
//        [_manager setReconnects:YES];
    }
    return self;
}

- (instancetype)init {
    return [self initWithURLPath:socketUrlPath];
}

- (void)connectToSocket {
    [self.socket on:@"connect" callback:^(NSArray* data, SocketAckEmitter* ack) {
        NSLog(@"socket connected");
//        dispatch_async(self.manager.engine.engineQueue, ^{
//            self.manager.engine.extraHeaders = @{@"user-agent" : @"atul manwar"};
//        });

        //[self.socket emit:@"test_location" with:@[@(12.3454353), @(77.23423423)]];
    }];

    [self.socket on:@"currentAmount" callback:^(NSArray* data, SocketAckEmitter* ack) {
        double cur = [[data objectAtIndex:0] floatValue];

        [[self.socket emitWithAck:@"canUpdate" with:@[@(cur)]] timingOutAfter:0 callback:^(NSArray* data) {
            [self.socket emit:@"update" with:@[@{@"amount": @(cur + 2.50)}]];
        }];

        [ack with:@[@"Got your currentAmount, ", @"dude"]];
    }];
//
    [self.socket on:@"disconnect" callback:^(NSArray * _Nonnull data, SocketAckEmitter * _Nonnull ack) {
        __weak SocketIOManager *strongSelf = self;
        [ZDRPeriodicTask scheduleBlock:^(ZDRPeriodicTask *task) {
            if (!strongSelf || strongSelf.isTeardown) {
                return;
            }
            [strongSelf.socket setReconnectingWithReason:@"reconnect"];
        } onQueue:dispatch_get_main_queue() afterDelaySeconds:0];
    }];

    [self.socket on:@"error" callback:^(NSArray * _Nonnull data, SocketAckEmitter * _Nonnull ack) {
        __weak SocketIOManager *strongSelf = self;
        [ZDRPeriodicTask scheduleBlock:^(ZDRPeriodicTask *task) {
            if (!strongSelf || strongSelf.isTeardown) {
                return;
            }
            [strongSelf.socket setReconnectingWithReason:@"reconnect"];
        } onQueue:dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0) afterDelaySeconds:0];
    }];

    [self.socket on:@"client_event" callback:^(NSArray * _Nonnull data, SocketAckEmitter * _Nonnull ack) {
        ClientEvent *clientEvent = [[ClientEvent alloc] init];
        clientEvent.coordinate = CLLocationCoordinate2DMake([data[0] doubleValue], [data[1] doubleValue]);
        clientEvent.priority = [data[2] intValue];
        clientEvent.eventDescription = data[3];
        clientEvent.expirationTime = [data[4] intValue];
        [self.controller clientEventReceived:clientEvent];
    }];

    [self.socket connect];
}

- (void)disconnect {
    _isTeardown = YES;
    [self.socket off:@"disconnect"];
    [self.socket off:@"error"];
    [self.socket disconnect];
}

- (void)sendToChannel:(NSString *)channel data:(NSArray *)data {
    [self.socket emit:channel with:data completion:^{
        NSLog(@"posted to %@!", channel);
    }];
}


@end
