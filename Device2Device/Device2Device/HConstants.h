//
//  HConstants.h
//  Device2Device
//
//  Created by Sudeep Kumar on 18/01/19.
//  Copyright © 2019 Zendrive Inc. All rights reserved.
//

#ifndef HConstants_h
#define HConstants_h

#define DEVICE_EVENT_CHANNEL @"device_event"
#define DEVICE_LOCATION_CHANNEL @"device_location"

const static double SPAN_PER_HALF_MILE = 0.00725f;

#define BEGIN_LOCATION CLLocationCoordinate2DMake(40.648748, -73.9545067)


#endif /* HConstants_h */
