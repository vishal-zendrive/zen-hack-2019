//
//  Utils.h
//  Device2Device
//
//  Created by Sudeep Kumar on 18/01/19.
//  Copyright © 2019 Zendrive Inc. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <MapKit/MapKit.h>


NS_ASSUME_NONNULL_BEGIN

@interface Utils : NSObject

+ (CLLocationCoordinate2D)getNorth:(CLLocationCoordinate2D)coordinates
                          distance:(int)distanceMeters;

+ (CLLocationCoordinate2D)getSouth:(CLLocationCoordinate2D)coordinates
                          distance:(int)distanceMeters;

+ (CLLocationCoordinate2D)getEast:(CLLocationCoordinate2D)coordinates
                         distance:(int)distanceMeters;

+ (CLLocationCoordinate2D)getWest:(CLLocationCoordinate2D)coordinates
                         distance:(int)distanceMeters;

@end

NS_ASSUME_NONNULL_END
