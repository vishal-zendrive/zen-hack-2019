//
//  ConsumerViewController.m
//  Device2Device
//
//  Created by Atul Manwar on 18/01/19.
//  Copyright © 2019 Zendrive Inc. All rights reserved.
//

#import "ConsumerViewController.h"
#import "SocketIOManager.h"
#import "HConstants.h"
#import "Utils.h"
#import "ZDRPeriodicTask.h"
#import <MapKit/MapKit.h>



@interface ConsumerViewController ()

@property (weak, nonatomic) IBOutlet MKMapView *mapView;

@property (nonatomic, strong) SocketIOManager* manager;

@property (weak, nonatomic) IBOutlet UIButton *upButton;
@property (weak, nonatomic) IBOutlet UIButton *leftButton;
@property (weak, nonatomic) IBOutlet UIButton *rightButton;
@property (weak, nonatomic) IBOutlet UIButton *downButton;

@property (weak, nonatomic) IBOutlet UISlider *speedSlider;
@property (weak, nonatomic) IBOutlet UILabel *speedLabel;
@property (nonatomic) ZDRPeriodicTask *longPressTask;
@property (nonatomic) ZDRPeriodicTask *locationSendTask;

@property (nonatomic) MKPointAnnotation *presentLocationAnnotation;

@property (nonatomic) int speed;
@end

@implementation ConsumerViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)viewDidAppear:(BOOL)animated {
    _presentLocationAnnotation = [[MKPointAnnotation alloc] init];
    self.speed = (int)self.speedSlider.value;

    self.manager = [[SocketIOManager alloc] init];
    self.manager.controller = self;
    [self connectToSocket];
    [self addLongPressGestures];
    [self moveToBeginLocation];
    [self startLocationSendTask];
}

- (void)viewWillDisappear:(BOOL)animated {
//    [self.mapView removeAnnotation:_presentLocationAnnotation];
//    _presentLocationAnnotation = nil;
    [self disableLocationSendTask];
    [self.manager disconnect];
    self.manager = nil;
    _upButton.gestureRecognizers = nil;
    _downButton.gestureRecognizers = nil;
    _leftButton.gestureRecognizers = nil;
    _rightButton.gestureRecognizers = nil;
}

- (void)startLocationSendTask {
    __weak ConsumerViewController *strongSelf = self;
    self.locationSendTask = [ZDRPeriodicTask scheduleBlock:^(ZDRPeriodicTask *task) {
        if (!strongSelf) {
            return;
        }
        [strongSelf sendCurrentLocation];
    } withPeriodSeconds:2 onQueue:dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0) afterDelaySeconds:0];
}

- (void)disableLocationSendTask {
    [self.locationSendTask cancel];
    self.locationSendTask = nil;
}

- (void)connectToSocket {
    [self.manager connectToSocket];
}

- (void)moveToBeginLocation {
    [self moveToLocation:BEGIN_LOCATION];
}

- (void)moveToLocation:(CLLocationCoordinate2D)location {
    float spanX = SPAN_PER_HALF_MILE * 2;
    float spanY = SPAN_PER_HALF_MILE * 2;

    MKCoordinateRegion region = self.mapView.region;
    region.center = location;
    region.span = MKCoordinateSpanMake(spanX, spanY);

    [self.mapView.userLocation setCoordinate:region.center];
    [self.mapView setRegion:region animated:YES];
    self.presentLocationAnnotation.coordinate = location;
    [self.mapView addAnnotation:self.presentLocationAnnotation];
}

- (CLLocationCoordinate2D)getCurrentLocation {
    return self.presentLocationAnnotation.coordinate;
}


- (IBAction)upButtonPressed:(id)sender {
    CLLocationCoordinate2D currLoc = [self getCurrentLocation];
    CLLocationCoordinate2D newLoc = [Utils getNorth:currLoc distance:self.speed];
    [self moveToLocation:newLoc];
}

- (IBAction)leftButtonPressed:(id)sender {
    CLLocationCoordinate2D currLoc = [self getCurrentLocation];
    CLLocationCoordinate2D newLoc = [Utils getWest:currLoc distance:self.speed];
    [self moveToLocation:newLoc];
}

- (IBAction)rightButtonPressed:(id)sender {
    CLLocationCoordinate2D currLoc = [self getCurrentLocation];
    CLLocationCoordinate2D newLoc = [Utils getEast:currLoc distance:self.speed];
    [self moveToLocation:newLoc];
}

- (IBAction)downButtonPressed:(id)sender {
    CLLocationCoordinate2D currLoc = [self getCurrentLocation];
    CLLocationCoordinate2D newLoc = [Utils getSouth:currLoc distance:self.speed];
    [self moveToLocation:newLoc];
}

- (IBAction)sliderValueChanges:(id)sender {
    int value = (int)self.speedSlider.value;
    self.speed = value;
    self.speedLabel.text = [NSString stringWithFormat:@"Speed:%d/click", self.speed];
}

- (void)addLongPressGestures {
    [self addLongPressGestureTo:self.upButton];
    [self addLongPressGestureTo:self.downButton];
    [self addLongPressGestureTo:self.leftButton];
    [self addLongPressGestureTo:self.rightButton];
}

- (void)addLongPressGestureTo:(UIButton *)button {
    UILongPressGestureRecognizer *longPress = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(longPress:)];
    [button addGestureRecognizer:longPress];
}

- (void)longPress:(UILongPressGestureRecognizer *)gesture {
    if ( gesture.state == UIGestureRecognizerStateEnded ) {
        [self.longPressTask cancel];
        self.longPressTask = nil;
        return;
    }

    __weak ConsumerViewController *strongSelf = self;
    if (_upButton.gestureRecognizers.firstObject == gesture) {
        [self schedule:^(ZDRPeriodicTask *task) {
            if (!strongSelf) {
                return;
            }
            [self upButtonPressed:nil];
        }];
    }
    else if (_leftButton.gestureRecognizers.firstObject == gesture) {
        [self schedule:^(ZDRPeriodicTask *task) {
            if (!strongSelf) {
                return;
            }
            [self leftButtonPressed:nil];
        }];
    }
    else if (_rightButton.gestureRecognizers.firstObject == gesture) {
        [self schedule:^(ZDRPeriodicTask *task) {
            if (!strongSelf) {
                return;
            }
            [self rightButtonPressed:nil];
        }];
    }
    else if (_downButton.gestureRecognizers.firstObject == gesture) {
        [self schedule:^(ZDRPeriodicTask *task) {
            if (!strongSelf) {
                return;
            }
            [self downButtonPressed:nil];
        }];
    }
}

- (void)schedule:(TaskBlock)block {
    self.longPressTask = [ZDRPeriodicTask scheduleBlock:block
                                      withPeriodSeconds:0.05
                                                onQueue:dispatch_get_main_queue()
                                      afterDelaySeconds:0];
}

- (void)sendCurrentLocation {
    CLLocationCoordinate2D currLoc = [self getCurrentLocation];
    [self.manager sendToChannel:DEVICE_LOCATION_CHANNEL data:@[[UIDevice currentDevice].identifierForVendor.UUIDString,
                                                               @(currLoc.latitude),
                                                               @(currLoc.longitude)]];
}

- (void)clientEventReceived:(ClientEvent *)clientEvent {
    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:
                              @"Event" message:clientEvent.eventDescription delegate:self
                                              cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
    [alertView show];
}


- (void)dealloc {
    [self locationSendTask];
    [self.longPressTask cancel];
    self.longPressTask = nil;
}

@end
