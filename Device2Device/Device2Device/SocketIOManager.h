//
//  SocketIOManager.h
//  Device2Device
//
//  Created by Atul Manwar on 18/01/19.
//  Copyright © 2019 Zendrive Inc. All rights reserved.
//

#import <Foundation/Foundation.h>
@import SocketIO;

NS_ASSUME_NONNULL_BEGIN

@class ConsumerViewController;
@interface SocketIOManager : NSObject
@property (nonatomic, strong, readonly) SocketIOClient* socket;
@property (nonatomic, strong, readonly) SocketManager* manager;
@property (nonatomic, weak) ConsumerViewController *controller;
@property (nonatomic) BOOL isTeardown;

- (instancetype)initWithURLPath:(NSString * _Nonnull)path;
- (instancetype)init;
- (void)connectToSocket;
- (void)disconnect;
- (void)sendToChannel:(NSString *)channel
                 data:(NSArray *)data;


@end

NS_ASSUME_NONNULL_END
