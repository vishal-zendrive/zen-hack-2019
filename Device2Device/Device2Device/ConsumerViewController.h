//
//  ConsumerViewController.h
//  Device2Device
//
//  Created by Atul Manwar on 18/01/19.
//  Copyright © 2019 Zendrive Inc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ClientEvent.h"

NS_ASSUME_NONNULL_BEGIN

@interface ConsumerViewController : UIViewController <UIGestureRecognizerDelegate>

- (void)clientEventReceived:(ClientEvent *)clientEvent;

@end

NS_ASSUME_NONNULL_END
