//
//  HGestureRecognizer.h
//  Device2Device
//
//  Created by Sudeep Kumar on 18/01/19.
//  Copyright © 2019 Zendrive Inc. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>

NS_ASSUME_NONNULL_BEGIN

@protocol TapLocationDelegate;
@interface HGestureRecognizer : NSObject <UIGestureRecognizerDelegate>

@property (nonatomic, weak) id<TapLocationDelegate> tapDelegate;

- (instancetype)initWithDelegate:(id<TapLocationDelegate>)tapDelegate
                      andMapView:(MKMapView *)mapView;

@end

NS_ASSUME_NONNULL_END
