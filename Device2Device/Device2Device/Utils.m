//
//  Utils.m
//  Device2Device
//
//  Created by Sudeep Kumar on 18/01/19.
//  Copyright © 2019 Zendrive Inc. All rights reserved.
//

#import "Utils.h"

@implementation Utils

+ (CLLocationCoordinate2D)getNorth:(CLLocationCoordinate2D)coordinates
                          distance:(int)distanceMeters {
    return [self start:coordinates up:distanceMeters right:0];
}


+ (CLLocationCoordinate2D)getSouth:(CLLocationCoordinate2D)coordinates
                          distance:(int)distanceMeters {
    return [self start:coordinates up:-distanceMeters right:0];
}

+ (CLLocationCoordinate2D)getEast:(CLLocationCoordinate2D)coordinates
                         distance:(int)distanceMeters {
    return [self start:coordinates up:0 right:distanceMeters];
}


+ (CLLocationCoordinate2D)getWest:(CLLocationCoordinate2D)coordinates
                         distance:(int)distanceMeters {
    return [self start:coordinates up:0 right:-distanceMeters];
}

+ (CLLocationCoordinate2D)start:(CLLocationCoordinate2D)coordinates
                             up:(int)distanceUp
                          right:(int)distanceRight {
    long long R = 6378137;//Earth’s radius, sphere
    double PI = 3.14159265358;

    int dn = distanceUp;
    int de = distanceRight;

    double dLat = (1.0*dn)/R;
    double dLon = de/(R*cos(PI*coordinates.latitude/180.0));

    double nLat = coordinates.latitude + dLat * 180/PI;
    double nLon = coordinates.longitude + dLon * 180/PI;

    return CLLocationCoordinate2DMake(nLat, nLon);
}


@end
