//
//  Models.swift
//  GodView
//
//  Created by Atul Manwar on 18/01/19.
//  Copyright © 2019 Zendrive Inc. All rights reserved.
//

import Foundation
import CoreLocation

struct Device {
    let id: String
    let coordinate: CLLocationCoordinate2D
    let topLeftCoordinate: CLLocationCoordinate2D
    let bottomRightCoordinate: CLLocationCoordinate2D

//    let speed: Double

    init(_ items: [Any]) {
        id = (items[0] as? String) ?? UUID().uuidString
        coordinate = CLLocationCoordinate2D(latitude: (items[1] as? Double) ?? 0, longitude: (items[2] as? Double) ?? 0)
        topLeftCoordinate = CLLocationCoordinate2D(latitude: (items[3] as? Double) ?? 0, longitude: (items[4] as? Double) ?? 0)
        bottomRightCoordinate = CLLocationCoordinate2D(latitude: (items[5] as? Double) ?? 0, longitude: (items[6] as? Double) ?? 0)
//        speed = (items[0] as? Double) ?? 0
    }
}

extension Device: Equatable {
}

func ==(lhs: Device, rhs: Device) -> Bool {
    return lhs.id == rhs.id
}

struct Event {
    let id: String
    let coordinate: CLLocationCoordinate2D
    let radius: Double
    let priority: Int
    let description: String

    init(_ items: [Any]) {
        id = (items[0] as? String) ?? UUID().uuidString
        coordinate = CLLocationCoordinate2D(latitude: (items[1] as? Double) ?? 0, longitude: (items[2] as? Double) ?? 0)
        radius = (items[3] as? Double) ?? 0
        priority = (items[4] as? Int) ?? 0
        description = (items[5] as? String) ?? ""
    }
}

extension Event: Equatable {
}

func ==(lhs: Event, rhs: Event) -> Bool {
    return lhs.id == rhs.id
}

enum AnnotationType {
    case device(Device)
    case event(Event)
}

extension AnnotationType: Equatable {
}

func ==(lhs: AnnotationType, rhs: AnnotationType) -> Bool {
    switch (lhs, rhs) {
    case (let .device(deviceLHS), let .device(deviceRHS)):
        return deviceLHS == deviceRHS
    case (let .event(eventLHS), let .event(eventRHS)):
        return eventLHS == eventRHS
    default:
        return false
    }
}

enum GodviewError: Error {
    case decodingError
}

struct Polyline {
    static func decodePolyline(_ encodedPolyline: String, precision: Double = 1e5) -> [CLLocationCoordinate2D] {
        guard let data = encodedPolyline.data(using: String.Encoding.utf8) else {
            return []
        }
        let byteArray = (data as NSData).bytes.assumingMemoryBound(to: Int8.self)
        let length = Int(data.count)
        var position = Int(0)
        var decodedCoordinates = [CLLocationCoordinate2D]()
        var lat = 0.0
        var lon = 0.0

        while position < length {
            do {
                let resultingLat = try decodeSingleCoordinate(byteArray: byteArray,
                                                              length: length,
                                                              position: &position,
                                                              precision: precision)
                lat += resultingLat
                let resultingLon = try decodeSingleCoordinate(byteArray: byteArray,
                                                              length: length,
                                                              position: &position,
                                                              precision: precision)
                lon += resultingLon
            } catch {
                return []
            }
            decodedCoordinates.append(CLLocationCoordinate2D(latitude: lat, longitude: lon))
        }
        return decodedCoordinates
    }

    static private func decodeSingleCoordinate(byteArray: UnsafePointer<Int8>,
                                               length: Int,
                                               position: inout Int,
                                               precision: Double = 1e5) throws -> Double {
        guard position < length else { throw GodviewError.decodingError }
        let bitMask = Int8(0x1F)
        var coordinate: Int32 = 0
        var currentChar: Int8
        var componentCounter: Int32 = 0
        var component: Int32 = 0

        repeat {
            currentChar = byteArray[position] - 63
            component = Int32(currentChar & bitMask)
            coordinate |= (component << (5*componentCounter))
            position += 1
            componentCounter += 1
        } while ((currentChar & 0x20) == 0x20) && (position < length) && (componentCounter < 6)
        if (componentCounter == 6) && ((currentChar & 0x20) == 0x20) {
            throw GodviewError.decodingError
        }
        if (coordinate & 0x01) == 0x01 {
            coordinate = ~(coordinate >> 1)
        } else {
            coordinate = coordinate >> 1
        }
        return Double(coordinate) / precision
    }

}

extension CLLocationCoordinate2D {
    fileprivate func degreesToRadians(_ degrees: Double) -> Double {
        return degrees * .pi / 180.0
    }

    fileprivate func radiansToDegrees(_ radians: Double) -> Double {
        return radians * 180.0 / .pi
    }

    func getBearing(for point: CLLocationCoordinate2D) -> Double {
        let lat1 = degreesToRadians(self.latitude)
        let lon1 = degreesToRadians(self.longitude)

        let lat2 = degreesToRadians(point.latitude)
        let lon2 = degreesToRadians(point.longitude)

        let dLon = lon2 - lon1

        let y = sin(dLon) * cos(lat2)
        let x = cos(lat1) * sin(lat2) - sin(lat1) * cos(lat2) * cos(dLon)
        let radiansBearing = atan2(y, x)

        return radiansToDegrees(radiansBearing)
    }
}
