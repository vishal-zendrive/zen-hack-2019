//
//  AnnotationView.swift
//  GodView
//
//  Created by Atul Manwar on 18/01/19.
//  Copyright © 2019 Zendrive Inc. All rights reserved.
//

import Foundation
import MapKit

class Annotation: MKPointAnnotation {
    var type: AnnotationType

    init(_ type: AnnotationType) {
        self.type = type
        super.init()
    }
}

class AnnotationView: MKAnnotationView {
//    fileprivate var rect: CGRect
//    fileprivate var view: NSView!

    init(annotation: MKAnnotation?, reuseIdentifier: String?, type: AnnotationType) {
//        self.rect = rect
        super.init(annotation: annotation, reuseIdentifier: reuseIdentifier)
        setup(type)
    }

    required init?(coder aDecoder: NSCoder) {
//        self.rect = .zero
        super.init(coder: aDecoder)
    }

    fileprivate func setup(_ type: AnnotationType) {
        switch type {
        case .event(let event):
            setupEventView(event)
        case .device(let device):
            setupDeviceView(device)
        }
    }

    fileprivate func setupDeviceView(_ device: Device) {
        image = NSImage(named: ["greenCar", "redCar"][Int.random(in: 0...1)])
//        if rect != .zero {
//            view.removeFromSuperview()
//            view = NSView(frame: rect)
//
//        }
    }

    fileprivate func setupEventView(_ event: Event) {
        image = NSImage(named: "danger")
    }
}

class DeviceOverlay: NSObject, MKOverlay {
    var coordinate: CLLocationCoordinate2D
    var boundingMapRect: MKMapRect
    var device: Device
    var identifier: String {
        return device.id
    }

    init(device: Device, rect: MKMapRect) {
        self.device = device
        boundingMapRect = rect
        coordinate = device.coordinate
    }
}

//class DevicePolygon: MKPolygon {
//    override init() {
//
//    }
//}
//
//class DevicePolygonRenderer: MKPolygonRenderer {
//    override init(polygon: MKPolygon) {
//        super.init(polygon: polygon)
//    }
//}

class DeviceOverlayRenderer: MKOverlayRenderer {
    override init(overlay: MKOverlay) {
        super.init(overlay: overlay)
    }
}
