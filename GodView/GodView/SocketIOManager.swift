//
//  SocketIOManager.swift
//  GodView
//
//  Created by Atul Manwar on 18/01/19.
//  Copyright © 2019 Zendrive Inc. All rights reserved.
//

import Cocoa
import SocketIO

class SocketIOManager: NSObject {
    private static let socketUrlPath = "http://9f6c2190.ngrok.io"

    fileprivate let manager: SocketManager
    fileprivate let socket: SocketIOClient

    init(urlPath: String, completion: @escaping (Bool) -> ()) {
        manager = SocketManager(socketURL: URL(string: urlPath)!,
                                config: [.log(true), .compress])
        manager.handleQueue = DispatchQueue.global(qos: .background)
        socket = manager.defaultSocket
        super.init()

        socket.on(clientEvent: .connect) { (_, _) in
            print("socket connected")
            completion(true)
        }

        socket.on(clientEvent: .error) { (_, _) in
            self.socket.setReconnecting(reason: "Reconnecting because of error")
        }

        socket.on(clientEvent: .disconnect) { (_, _) in
            self.socket.setReconnecting(reason: "Reconnecting because it disconnected")
        }

        socket.connect()
    }

    convenience init(_ completion: @escaping (Bool) -> ()) {
        self.init(urlPath: SocketIOManager.socketUrlPath,
                  completion: completion)
    }

    func subscribe(_ event: String, callback: @escaping NormalCallback) -> UUID {
        return socket.on(event,
                         callback: callback)
    }

    func unsubscribe(_ event: String) {
        socket.off(event)
    }
}
