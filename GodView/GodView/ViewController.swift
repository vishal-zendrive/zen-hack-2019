//
//  ViewController.swift
//  GodView
//
//  Created by Atul Manwar on 18/01/19.
//  Copyright © 2019 Zendrive Inc. All rights reserved.
//

import Cocoa
import MapKit
import SocketIO

class ViewController: NSViewController {
    @IBOutlet weak var mapView: MKMapView! = nil {
        didSet {
            mapView.showsCompass = false
            mapView.showsUserLocation = true
        }
    }
    fileprivate var manager: SocketIOManager!
    fileprivate let devicesChannel = "position_stream"
    fileprivate let eventsChannel = "event_stream"
    fileprivate let eventDeletionChannel = "event_deletion_stream"
    fileprivate var devicesTimer: Timer!
    fileprivate var locations: [CLLocationCoordinate2D] = []
    fileprivate var devices: [Device] = []
    fileprivate var events: [Event] = []
    fileprivate var firstReceived = false

    override func viewDidLoad() {
        super.viewDidLoad()
        mapView.delegate = self

        manager = SocketIOManager({ [weak self] (_) in
            guard let `self` = self else { return }
            self.subscribe()
        })

//        let locations = Polyline.decodePolyline("opg~Fh|dvOq@a{@_TFq^r@}K}UoYsg@qLyW_Zmy@cV_p@k[yw@Fgt@^yk@dWeIdRObUcAdVc@Yde@LnTPzS^vf@Ln}@Rv\\")
//
//        self.devices = locations.map { (location) in
//            return Device([
//                "deviceIdAtul",
//                location.latitude,
//                location.longitude,
//                0,
//                0,
//                1,
//                1])
//        }

//        devicesTimer = Timer.scheduledTimer(withTimeInterval: 1.5, repeats: true, block: { (timer) in
//            DispatchQueue.main.async {
//                if let device = self.devices.first {
//                    if !self.firstReceived {
//                        self.setMapCamera(device.coordinate)
//                        self.firstReceived = true
//                    }
//                    self.devicesDataReceived(device)
//                    self.devices.removeFirst()
//                }
//            }
//        })
//        devicesTimer.fire()
    }

    override func viewWillAppear() {
        super.viewWillAppear()
    }

    override func viewDidAppear() {
        super.viewDidAppear()
    }

    override func viewDidDisappear() {
        super.viewDidDisappear()
    }

    override var representedObject: Any? {
        didSet {
        // Update the view, if already loaded.
        }
    }

    fileprivate func subscribe() {
        _ = manager.subscribe(devicesChannel) { [weak self] (data, emitter) in
//            DispatchQueue.global(qos: .utility).async {
                guard let `self` = self else { return }
                let device = Device(data)
//                if !self.firstReceived {
//                    DispatchQueue.main.async {
//                        self.setMapCamera(device.coordinate)
//                    }
//                    self.firstReceived = true
//                }
//                self.devices.append(device)
//            }
                DispatchQueue.main.async {
                    if !self.firstReceived {
                        self.setMapCamera(device.coordinate)
                        self.firstReceived = true
                    }
                    self.devicesDataReceived(device)
                    //                    self.devices.removeFirst()
                }
            }
        _ = manager.subscribe(eventsChannel) { [weak self] (data, emitter) in
            guard let `self` = self else { return }
            let event = Event(data)
//            self.events.append(Event(data))
            if !self.firstReceived {
                DispatchQueue.main.async {
                    self.setMapCamera(event.coordinate)
                }
                self.firstReceived = true
            }
            self.eventsDataReceived(Event(data))
        }
        _ = manager.subscribe(eventDeletionChannel) { [weak self] (data, emitter) in
            guard let `self` = self, let eventId = data.first as? String else { return }
            DispatchQueue.main.async {
                if let annotation = self.mapView.annotations
                    .compactMap({ $0 as? Annotation})
                    .first(where: {
                        switch $0.type {
                        case .event(let event):
                            return (event.id == eventId)
                        case .device(_):
                            return false
                        }
                    }) {
                    self.mapView.removeAnnotation(annotation)
                }
            }
        }

    }

    fileprivate func devicesDataReceived(_ device: Device) {
        let deviceAnnotation = self.getAnnotation(AnnotationType.device(device))
        self.animateAnnotation(deviceAnnotation,
                          to: device.coordinate)
    }

    fileprivate func eventsDataReceived(_ event: Event) {
        DispatchQueue.main.async {
            let _ = self.getAnnotation(AnnotationType.event(event))
        }
    }
}

// MARK: MapView related methods
extension ViewController {
    fileprivate func getAnnotation(_ type: AnnotationType) -> Annotation {
        if let annotation = mapView.annotations
            .compactMap({ $0 as? Annotation})
            .first(where: { (type == $0.type) }) {
            annotation.type = type
            return annotation
        } else {
            let annotation = Annotation(type)
            mapView.addAnnotation(annotation)
            return annotation
        }
    }

    fileprivate func getReusableIdentifier(_ type: AnnotationType) -> String {
        switch type {
        case .device(let device):
            return device.id
        case .event(let event):
            return event.id
        }
    }

    fileprivate func animateAnnotation(_ annotation: Annotation,
                                       to coordinate: CLLocationCoordinate2D) {
        NSAnimationContext.current.allowsImplicitAnimation = true
        let bearing = annotation.coordinate.getBearing(for: coordinate)
        let annotationView = mapView.view(for: annotation)

        NSAnimationContext.runAnimationGroup({ (context) in
            context.duration = 0.5
            context.timingFunction = CAMediaTimingFunction(name: .easeOut)
            annotationView?.wantsLayer = true
//            annotationView?.layer?.setAffineTransform(CGAffineTransform(rotationAngle: CGFloat(bearing)))
//            annotationView?.boundsRotation = CGFloat(bearing)
            annotationView?.boundsRotation = CGFloat(bearing)
//            annotationView?.rotate(byDegrees: CGFloat(bearing))
        }, completionHandler: {
            NSAnimationContext.current.allowsImplicitAnimation = true
            NSAnimationContext.runAnimationGroup({ (context) in
                context.duration = 1
                context.timingFunction = CAMediaTimingFunction(name: .easeIn)
                annotation.coordinate = coordinate
            }, completionHandler: {
//            self.mapView.showAnnotations(self.mapView.annotations, animated: true)
            })
        })
    }

    fileprivate func setMapCamera(_ coordinate: CLLocationCoordinate2D) {
        let camera = MKMapCamera(lookingAtCenter: coordinate,
                                 fromDistance: 10000,
                                 pitch: 0, heading: 0)
        mapView.setCamera(camera, animated: false)
    }

    fileprivate func getMapCamera(_ coordinate: CLLocationCoordinate2D) -> MKMapCamera {
        return MKMapCamera(lookingAtCenter: coordinate,
                                 fromDistance: 10000,
                                 pitch: 0, heading: 0)
    }

}

extension ViewController: MKMapViewDelegate {
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        if let annotation = annotation as? Annotation {
            return AnnotationView(annotation: annotation,
                                  reuseIdentifier: getReusableIdentifier(annotation.type),
                                  type: annotation.type)
        } else {
            return nil
        }
    }
//
//    func mapView(_ mapView: MKMapView, rendererFor overlay: MKOverlay) -> MKOverlayRenderer {
//        if let overlay = overlay as? DeviceOverlay {
//            let overlayRenderer =  DeviceOverlayRenderer(overlay: overlay)
//            overlayRenderer.alpha = 0.8
//            return overlayRenderer
//        } else {
//            return MKOverlayRenderer(overlay: overlay)
//        }
//    }
}
